import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGUI {

    private JPanel root;
    private JButton HamburgerButton;
    private JButton KaisendonButton;
    private JButton KaraageTeisyokuButton;
    private JButton MisokatuniTeisyokuButton;
    private JButton SabaTeisyokuButton;
    private JButton ShogayakiTeisyokuButton;
    private JTextPane OrderedItems;
    private JButton CheckOutButton;
    private JLabel TotalYen;

    int total=0;
    void order(String food,int cost){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order "+food+"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if(confirmation == 0){
            JOptionPane.showMessageDialog(null,"Thank you ordering "+food+" ! It will be served as soon as possible.");
            String currentText = OrderedItems.getText();
            OrderedItems.setText(currentText + food + " " + cost + "yen\n");
            total += cost;
            TotalYen.setText("Total         " + total + " yen     ");
        }
    };

    public FoodGUI() {
        HamburgerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Hamburger",1150);
            }
        });
        HamburgerButton.setIcon(new ImageIcon( this.getClass().getResource("Hamburger.jpg")));

        KaisendonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Kaisendon",850);
            }
        });
        KaisendonButton.setIcon(new ImageIcon( this.getClass().getResource("Kaisendon.jpg")));

        KaraageTeisyokuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("KaraageTeisyoku",750);
            }
        });
        KaraageTeisyokuButton.setIcon(new ImageIcon( this.getClass().getResource("KaraageTeisyoku.jpg")));

        MisokatuniTeisyokuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("MisokatuniTeisyoku",800); }
        });
        MisokatuniTeisyokuButton.setIcon(new ImageIcon( this.getClass().getResource("MisokatuniTeisyoku.jpg")));

        SabaTeisyokuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("SabaTeisyoku",550);
            }
        });
        SabaTeisyokuButton.setIcon(new ImageIcon( this.getClass().getResource("SabaTeisyoku.jpg")));

        ShogayakiTeisyokuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("SyogayakiTeisyoku",650);
            }
        });
        ShogayakiTeisyokuButton.setIcon(new ImageIcon( this.getClass().getResource("ShogayakiTeisyoku.jpg")));

        CheckOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation == 0){
                    int confirmation2 = JOptionPane.showConfirmDialog(null,
                            "Are you a university student?",
                            "Student Discount Confirmation",
                            JOptionPane.YES_NO_OPTION);
                    if(confirmation2 == 0){
                        JOptionPane.showMessageDialog(null,
                                "Student discount of 150 yen will be applied.");
                        if(total > 150){
                            total -= 150;
                        }
                        else{
                            total = 0;
                        }
                    }
                    JOptionPane.showMessageDialog(null,
                            "Thank you. The total price is " + total + " yen.");
                    OrderedItems.setText("");
                    total = 0;
                    TotalYen.setText("Total             " + total + " yen     ");
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
